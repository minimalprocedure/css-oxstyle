# Brutalist Aestetics for monitora-pa.it

A brutalist theme designed by Massimo Maria Ghisalberti for https://monitora-pa.it (derived from http://codexpo.org/)

# Copyright

Copyright (c) 2022 Massimo Maria Ghisalberti <massimo.ghisalberti@pragmas.org>

Initial developer Massimo Maria Ghisalberti <massimo.ghisalberti@pragmas.org>

This source is released under the terms of the Hacking License.
The initial developer reserves the right to use the source and any derivative CSS or SCSS under other licenses (OSS/FOSS/FLOSS).

**This work, is under a dual license: MIT and Hacking. Choose the most suitable one.**

# Build

Given your output folder of choice set in a `$OUTPUT_FOLDER` variable

`sass ./main.scss:$OUTPUT_FOLDER/monito-rapa.css --style=expanded`

